using UnityEngine;

namespace Obsolete_LevelEditor.Global
{
    [CreateAssetMenu(fileName = nameof(Resources), menuName = "Obsolete_LevelEditor_V1/Resources")]
    public class Resources : ScriptableObject
    {
        public static Resources Instance { get { return UnityEngine.Resources.Load("Resources") as Resources; } }

        public LevelTextureSetting StandardLevelSetting;
        public Material DungeonCeilingMaterial;
        //public Material DungeonFloorMaterial;
        //public Material DungeonFloorMaterialEditor;

        public RoomLibrary Library;
    }
}
