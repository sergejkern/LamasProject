﻿using System.Collections.Generic;
using Core.Events;
using Core.Types;
using GameUtility.Interface;
using UnityEngine;

namespace Game
{
    public class QuestTracker : Singleton<QuestTracker>
    {
        readonly List<IQuest> m_activeQuests = new List<IQuest>();
        readonly List<IQuest> m_doneQuests = new List<IQuest>();

        public int ActiveQuestCount => m_activeQuests.Count;
        public int DoneQuestsCount => m_doneQuests.Count;

        public IEnumerable<IQuest> ActiveQuests => m_activeQuests;
        public IEnumerable<IQuest> DoneQuests => m_doneQuests;

        public void AddQuest(IQuest quest)
        {
            //Debug.Log($"AddQuest! {quest}");
            if (quest == null)
            {
                Debug.LogError("Quest is null");
                return;
            }   
            quest.OnQuestStarted();
            m_activeQuests.Add(quest);
            EventMessenger.TriggerEvent(new QuestStatusChanged(){ Quest = quest });
        }

        public void FinishQuest(IQuest quest)
        {
            quest.OnQuestComplete();
            m_activeQuests.Remove(quest);
            m_doneQuests.Add(quest);
            EventMessenger.TriggerEvent(new QuestStatusChanged(){ Quest = quest });
        }
    }

    public struct QuestStatusChanged
    {
        public IQuest Quest;
    }
}
