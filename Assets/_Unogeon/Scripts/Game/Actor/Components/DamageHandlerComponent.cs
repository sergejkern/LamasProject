﻿using Core.Unity.Extensions;
using Game.Actors.Controls;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using ScriptableUtility.Behaviour;
using ScriptableUtility.Graph;
using UnityEngine;

namespace Game.Actors.Components
{
    /// <summary>
    /// Used to handle damage on actors
    /// </summary>
    public class DamageHandlerComponent : MonoBehaviour, IDamageHandler
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_logicRoot;
        [SerializeField] ActionNameID m_onDamaged;

        [SerializeField] CollisionMaterialID m_materialId;
        [SerializeField] CollisionMaterialID m_absorbedMaterialID;
        [SerializeField] float m_minDamage;

        [SerializeField] float m_collisionPosCenterPercent;
        [SerializeField] Transform m_collisionPos;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ActionBehaviour m_actionBehaviour;
        ControlCentre m_controlCentre;

        EnergyCentre m_energyCentre;

        IDefense m_defenseControl;

        //public Vector3 LastDamagePos { get; private set; }

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        //public HealthComponent Health => m_healthComponent;
        public Vector3 CollisionPos => m_collisionPos == null ? transform.position : m_collisionPos.position;
        public Quaternion CollisionRotation =>  m_collisionPos == null ? transform.rotation : m_collisionPos.rotation;
        public float CollisionPosCenterPercent => m_collisionPosCenterPercent;
        public CollisionMaterialID Material => m_materialId;
        
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        void Awake()
        {
            if (m_logicRoot == null)
                return;

            m_logicRoot.TryGet(out m_controlCentre);
            m_logicRoot.TryGet(out m_actionBehaviour);
            if (m_controlCentre != null)
                m_controlCentre.TryGet(out m_defenseControl);

            m_logicRoot.TryGet(out m_energyCentre);
        }

        public void HandleDamage(IDamageData damage, ref HandleDamageResult result)
        {
            // LastDamagePos = result.DamagePos;
            if (m_defenseControl != null
                && m_defenseControl.IsBlocking(damage, out var colMat))
            {
                result.Material = colMat;
                result.Result = DamageResult.Blocked;
                return;
            }

            var energy = m_energyCentre.GetEnergyComponent(damage.DamageOnEnergyID);
            if (energy != null && (energy.IsInvulnerable || energy.Energy <= 0f))
            {
                result.Result = DamageResult.Ignored;
                return;
            }

            if (damage.DamageAmount < m_minDamage)
            {
                result.DamageDone = 0;
                result.DamageAbsorbed = damage.DamageAmount ;
                result.Result = DamageResult.Absorbed;
                result.Material = m_absorbedMaterialID;
                return;
            }
            
            var damageAmount = 1f;

            if (energy != null)
            {
                damageAmount = Mathf.Min(damage.DamageAmount, energy.Energy);
                energy.Damage(damage.DamageAmount);
            }
            // Debug.Log($"Applying Damage {damage} reduced to {damageAmount} HP: {m_healthComponent.HP}");

            result.Result = (energy == null || energy.Energy <= 0f) 
                ? DamageResult.Killed 
                : DamageResult.Damaged;
            result.DamageDone = damageAmount;

            if (m_actionBehaviour != null)
                m_actionBehaviour.InvokeAction(m_onDamaged);
        }
    }
}

//IMemorizeMaterials m_memoryAC;
//readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
//IEnumerable<MaterialMemory> Memory => m_memoryAC != null ? m_memoryAC.Memory : m_memory;
//m_logicRoot.TryGet(out IModel model);
//m_logicRoot.TryGet(out m_memoryAC);
//m_logicRoot.TryGet(out m_animator);
// todo: put this to MaterialMemory-component shit
//void OnDisable() => MaterialExchange.Revert(Memory);
//[SerializeField] AnimatorStateRef m_hitAnimatorState;
//[SerializeField, FSMDrawer(setToSelf: Condition.Never)] 
//FSMEventRef m_onHit;
//[SerializeField] bool m_rumble;
//[SerializeField] [ShowIf("m_rumble")]
//RumbleData m_rumbleData;
//[SerializeField] bool m_freezeAnimation;
//[SerializeField] [ShowIf("m_freezeAnimation")]
//float m_freezeTime;
//[SerializeField] bool m_flash;
//[SerializeField, ShowIf("m_flash")]
//FlashDataNoTarget m_flashData;
//GameObject m_model;
//Animator m_animator;
////! first hit-animation then damage, otherwise no death-animation
//// we don't check if we currently attacking, because attacking is expected on different layer
//// and will override hit-animation anyway
//if (Animator != null)
//    Animator.Play(m_hitAnimatorState);
//m_onHit.Invoke();
//if (m_flash)
//{
//    m_flashData.Memory = Memory;
//    StartCoroutine(FlashByMaterialExchangeRoutine.Flash(m_flashData, Model));
//}
//if (m_rumble && m_controlCentre != null)
//    m_controlCentre.SetRumble(m_rumbleData);
//if (m_freezeAnimation)
//    StartCoroutine(FreezeAnimation(m_freezeTime));
//IEnumerator FreezeAnimation(float seconds)
//{
//    var animator = Animator;
//    if (animator == null)
//        yield break;
//    var prevSpeed = animator.speed;
//    animator.speed = 0f;
//    yield return new WaitForSeconds(seconds);
//    // ReSharper disable once Unity.InefficientPropertyAccess
//    animator.speed = prevSpeed;
//}