﻿using Core.Events;
using Core.Unity.Extensions;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Actors.Energy
{
    public class FoodEnergy : EnergyComponent, IEventListener<AttachmentPointsUpdated>
    {
        [SerializeField] AttachmentID m_bellyID;
        [SerializeField] Vector3 m_fromScale;
        [SerializeField] Vector3 m_toScale;

        Transform bellyTr;

        void OnEnable()
        {
            EventMessenger.AddListener(this);
            if (m_entity.Result.gameObject.TryGet(out IAttachmentPoints ap))
                UpdateAttachmentPoints(ap);
        }

        void OnDisable() => EventMessenger.RemoveListener(this);
        protected override void Update()
        {
            base.Update();

            if (bellyTr == null)
                return;
            bellyTr.localScale = Vector3.Lerp(m_fromScale, m_toScale, EnergyPercentage);
        }

        
        public void OnEvent(AttachmentPointsUpdated @event)
        {
            if (@event.ForEntity == m_entity.Result)
                UpdateAttachmentPoints(@event.Provider);
        }
        void UpdateAttachmentPoints(IAttachmentPoints prov) 
            => bellyTr = prov.GetAttachment(m_bellyID).Parent;
    }
}
