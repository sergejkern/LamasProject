﻿using System;
using System.Collections.Generic;
using Core.Events;
using GameUtility.Data.TransformAttachment;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Actors.Model_Animation
{
    public class ModelComponent : MonoBehaviour, IModel, IEventListener<ModelLinkedEvent>
    {
        public IEnumerable<Type> ModuleTypes => new[] { typeof(Animator), typeof(IAttachmentPoints) };

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Animator m_animator;

        [Tooltip("Link the Collider object here")]
        [SerializeField] ColliderParent m_colliderParent;

        [SerializeField] AttachmentPointsComponent m_attachmentPointsComponent;

        [Tooltip("Link RelativeCollider.SetAssociatedObject() here to link the logic GameObject")]
        [SerializeField] UnityEvent<GameObject> m_connectLogicActions;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnEnable() => EventMessenger.AddListener(this);
        void OnDisable() => EventMessenger.RemoveListener(this);
        public void Connect(GameObject logic) { m_connectLogicActions.Invoke(logic); }

        public void Get(out ColliderParent provided) => provided = m_colliderParent;
        public void Get(out Animator provided) => provided = m_animator;
        public void Get(out AttachmentPointsComponent provided) => provided = m_attachmentPointsComponent;

        
        public void OnEvent(ModelLinkedEvent @event)
        {
            if (!ReferenceEquals(@event.Model, this))
                return;
            Connect(@event.LogicRoot.gameObject);
        }

        #if UNITY_EDITOR
        public void Editor_Sync(GameObject prefabRoot)
        {
            m_animator = prefabRoot.GetComponentInChildren<Animator>();
            m_attachmentPointsComponent = prefabRoot.GetComponentInChildren<AttachmentPointsComponent>();
        }
        #endif
    }

    public struct ModelLinkedEvent
    {
        public ILogicEntity LogicRoot;
        public IModel Model;
    }
}
