﻿using Game.Dialogue;
using Game.Utility.NameGenerator;
using UnityEngine;

namespace Game.Actor
{
    public class NPCCharacter : MonoBehaviour, ICharacter
    {
        [SerializeField] NameGeneratorConfig m_nameGeneratorConfig;

        void Awake() => Name = m_nameGeneratorConfig.GetRandomName();
    
        public string Name { get; private set; }
    }
}
