using System.Collections.Generic;
using Game.InteractiveObjects;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class DropItemControl : MonoBehaviour, IInputActionReceiver, IInvokable 
        //IInitDataComponent<UseItemControl.ConfigData>,
    {
        [FormerlySerializedAs("m_equipmentProvider")] [SerializeField] RefIEquipment m_equipment;
        [SerializeField] AttachmentID m_id;

        [SerializeField] InputToFSMTrigger m_trigger;

        IGrabbable Equip => m_equipment.Result.GetEquipment(m_id);

        public bool Enabled => enabled;
        public bool CanInvoke => Equip != null;

        public ControlCentre ControlCentre { get; private set; }


        void Awake() => Init();

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;

            if (m_trigger.Initialized)
                return;
            m_trigger.Init(ControlCentre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled,
                Invokable = Invoke
            }, InputEnergyData.Default);
        }

        public void Invoke() => DropItem();
        public void DropItem() => m_equipment.Result.Drop(m_id);

        public void OnEquipChanged(IEquipment equipment) 
            => enabled = CanInvoke;

        public IEnumerable<ActionMapping> Mapping => new ActionMapping[]
        {
            new ActionMapping()
            {
                ActionRef = m_trigger.ActionRef,
                Call = m_trigger.OnControl,
            }, 
        };
    }
}