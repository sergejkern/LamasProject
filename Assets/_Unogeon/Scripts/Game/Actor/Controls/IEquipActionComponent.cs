

using Game.InteractiveObjects;
using GameUtility.Data.Input;

namespace Game.Actors.Controls
{
    public interface IEquipActionComponent : IInputActionReceiver
    {
        EquipActionType ActionForActionType { get; }
        void OnEquipChanged(IEquipment equipment);
    }
}