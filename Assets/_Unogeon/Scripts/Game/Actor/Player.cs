using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Interface;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Game.Actors.Controls;
using Game.Actors.Model_Animation;
using Game.Globals;
using Game.Interactive;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using GameUtility.Interface;
using ScriptableUtility;
using UnityEngine;
using UnityEngine.InputSystem;
using IModel = GameUtility.Interface.IModel;

namespace Game.Actors
{
    public class Player: MonoBehaviour, IPlayer, 
        IProvider<ISpawnAPrefab>,
        IProvider<IEquipment>,
        IProvider<IInventory>, 
        IVerify //, IDamagedByActorMarker
    {
        public IEnumerable<Type> ModuleTypes => new[]
        {
            typeof(ILogicEntity),
            typeof(IContext), 
            typeof(IMutableTransformData),
            typeof(ISpawnAPrefab), // <-- model
            typeof(IControlCentre),
            typeof(IEnergyCentre),
            typeof(IAttachmentPoints),
            typeof(IEquipment),
            typeof(IInventory),
        };

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIControllableCentre m_controlCentre;
        [SerializeField] RefIEnergyCentre m_energyCentre;
        [SerializeField] RefIAttachmentPoints m_attachmentPoints;
        [SerializeField] RefISpawnAPrefab m_modelSpawner;
        [SerializeField] RefIEquipment m_equipment;
        [SerializeField] RefIInventory m_inventory;
#pragma warning restore 0649

        #region Properties and Provider
        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        public IControlCentre Centre => m_controlCentre.Result;
        // todo check ControlCentre and Controllable
        public IControllable Controllable => m_controlCentre.Result;

        public PlayerInput Input => Centre.Input;
        public bool IsBeingControlled => Input != null;

        IEnergyCentre EnergyCentre => m_energyCentre.Result;
        IAttachmentPoints AttachmentPoints => m_attachmentPoints.Result;
        ISpawnAPrefab ModelSpawner => m_modelSpawner.Result;
        IEquipment Equipment => m_equipment.Result;
        IInventory Inventory => m_inventory.Result;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible

        // player nr. 0-3
        public int PlayerIdx { get; private set; }

        // ReSharper disable MethodNameNotMeaningful
        public void Get(out IEnergyCentre provided) => provided = m_energyCentre.Result;
        public void Get(out IInventory provided) => provided = m_inventory.Result;
        public void Get(out IEquipment provided) => provided = m_equipment.Result;
        public void Get(out IControlCentre provided) => provided = m_controlCentre.Result;
        public void Get(out IModel provided) => provided = m_model;
        public void Get(out IAttachmentPoints provided) => provided = m_attachmentPoints.Result;
        public void Get(out ISpawnAPrefab provided) => provided = m_modelSpawner.Result;
        public void Get(out Animator provided) => provided = m_animator;
        // ReSharper restore MethodNameNotMeaningful
        #endregion

        Animator m_animator;
        //ControlBlocker m_controlBlockerDeath;
        IModel m_model;

        #region Init
        void Awake()
        {
            //m_controlBlockerDeath = new ControlBlocker() { Source = this };
            CreateAndLinkModel();
            //InitAgent();
        }

        void CreateAndLinkModel()
        {
            var modelGo = Instantiate(ModelSpawner.Prefab, transform);
            modelGo.TryGet(out IModel model);
            if (m_model != null)
            {
                Debug.LogError("Model already instantiated!!");
                return;
            }
                
            m_model = model;
            modelGo.TryGet(out m_animator);
            modelGo.TryGet<IAttachmentPoints>(out var modelAttachments);

            if (AttachmentPoints is AttachmentPointsProvider prov)
                prov.AddAttachmentPoints(modelAttachments);

            EventMessenger.TriggerEvent(new ModelLinkedEvent() { LogicRoot = this, Model = m_model });
        }

        //void InitAgent()
        //{
        //    if (!m_controlCentre.TryGetComponent<NavMeshAgent>(out var agent))
        //        return;
        //    agent.updatePosition = false;
        //    agent.updateRotation = false;
        //}

        public void SetPlayerIdx(int playerIdx) => PlayerIdx = playerIdx;
        #endregion

        #region IControllable methods
        public void InitializeWithController(PlayerInput input)
        {
            Controllable.InitializeWithController(input);
            GlobalOperations.MovePlayerToRuntimeSceneAndConnectHUD(this);
            // m_playerFXComponent.Connect(this);
            // m_events.OnInputConnected.Invoke();
        }

        public void RemoveControl() => Controllable.RemoveControl();
        public void AddControlBlocker(ControlBlocker blocker) => Controllable.AddControlBlocker(blocker);
        public void RemoveControlBlocker(ControlBlocker blocker)=> Controllable.RemoveControlBlocker(blocker);
        //public void RemoveControlBlocker(object source) => m_controlCentre.RemoveControlBlocker(source);
        #endregion
       
        #region IPoolable methods
        public void OnSpawn() {}
        //enabled = true;
        public void OnDespawn() {}
        //{
        //    m_animator.Play(m_animations.DefaultStateRef);
        //    m_events.OnDespawn.Invoke();
        //}
        #endregion

        #region UNITY_EDITOR
#if UNITY_EDITOR
        public void Editor_Sync(GameObject prefabRoot)
        {
            if (Centre == null)
                m_controlCentre.Result = prefabRoot.GetComponentInChildren<IControllableCentre>();
            
            if (EnergyCentre == null)
                m_energyCentre.Result = prefabRoot.GetComponentInChildren<IEnergyCentre>();
            
            if (AttachmentPoints == null)
                m_attachmentPoints.Result = prefabRoot.GetComponentInChildren<IAttachmentPoints>();
            
            if (ModelSpawner == null)
                m_modelSpawner.Result = prefabRoot.GetComponentInChildren<ISpawnAPrefab>();

            if (Inventory == null)
                m_inventory.Result = prefabRoot.GetComponentInChildren<IInventory>();

            if (Equipment == null)
                m_equipment.Result = prefabRoot.GetComponentInChildren<IEquipment>();
        }
        public void Editor_Verify(ref VerificationResult result)
        {
            if (Centre == null)
                result.Error($"Player is missing 'Control Centre'!", this);
            if (EnergyCentre == null)
                result.Error($"Player is missing 'Energy Centre'!", this);
            if (AttachmentPoints == null)
                result.Error($"Player is missing 'Attachment Points Provider'!", this);
            if (ModelSpawner == null)
                result.Error($"Player is missing 'Player Model Prefab'", this);
            else if (ModelSpawner.Prefab == null)
                result.Error($"'Player Model Prefab' has no prefab", this);
            else if (!ModelSpawner.Prefab.TryGet(out IModel _))
                result.Error($"prefab set in 'Player Model Prefab' is not of type {nameof(IModel)}", this);

            if (Equipment == null)
                result.Warning($"Player is missing 'Equipment'", this);
            if (Inventory == null)
                result.Warning($"Player is missing 'Inventory'", this);
        }
#endif
        #endregion
    }
}

//public void OnDeath()
//{
//// we don't actually know if last aggressor caused the kill, but we account him for it for the Results
//// (could also be that he fell, because of last attack or so)todo0:poor solution
//// KillEvent.Trigger(gameObject, LastAggressor);
//if (!enabled)
//return;
//enabled = false;
//m_events.OnDeathEvent.Invoke();
//m_animator.Play(m_animations.PlayerDeath);
//// gameObject.AddComponent<DecomposeBody>();
//AddControlBlocker(m_controlBlockerDeath);
//}
//public GameObject LastAggressor { get; set; }
//[Serializable]
//public struct Events
//{
//    public FSMUnityCombinedEvent OnDeathEvent;
//    public UnityEvent<GameObject> OnModelSpawned;
//    public UnityEvent OnDespawn;
//    public UnityEvent OnInputConnected;
//}
//[Serializable]
//public struct Animations
//{
//    [AnimTypeDrawer]
//    public AnimatorStateRef PlayerDeath;
//    [AnimTypeDrawer]
//    public AnimatorStateRef DefaultStateRef;
//}
//[SerializeField] Animations m_animations;
//[SerializeField] Events m_events;