﻿using Core.Interface;
using Core.Unity.Extensions;
using Game.Actors.Controls;
using GameUtility.Data.Item;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using GameUtility.Interface;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class Food: MonoBehaviour, IGrabbable, IEatable
    {
        [SerializeField] ItemID m_itemID;

        [SerializeField] EnergyID m_energyID;
        [SerializeField] float m_energy;
        [SerializeField] Sprite m_icon;

        [SerializeField] Collider m_collider;
        [SerializeField] Rigidbody m_rigidbody;

        // ReSharper disable once ConvertToAutoProperty
        public float Energy => m_energy;
        public bool InteractEnabled => m_owner==null;
        public GrabRestrictions GrabRestrictions => GrabRestrictions.None;
        public EquipActionType ActionType => EquipActionType.Item;
        public ItemID ItemID => m_itemID;
        public Sprite Icon => m_icon;

        public GameObject Owner
        {
            get => m_owner;
            private set
            {
                m_owner = value;
                //Debug.Log($"Set Owner {value}");
                m_collider.isTrigger = m_owner != null;
                m_rigidbody.isKinematic = m_owner != null;
            }
        }

        GameObject m_owner;

        public void OnGrabbed(IControlCentre grabbingActor) => Owner = grabbingActor.ControlledObject;

        public void AdjustPositioning(TransformData grabbingHand)
        {
            var tr = transform;
            tr.position = grabbingHand.Position;
            tr.rotation = grabbingHand.Rotation;
        }

        public void Drop() => Owner = null;

        public void Throw()
        {
            Drop();
            //throw new System.NotImplementedException();
        }

        public void Use(GameObject actor)
        {
            var energyCentre = actor.Get<IEnergyCentre>();
            var energyComponent = energyCentre.GetEnergyComponent(m_energyID);
            energyComponent.Regenerate(Energy);

            m_owner.TryGet(out IEquipment equipment);
            equipment.Drop(this);
            gameObject.TryDespawnOrDestroy();
        }

        public void SetOwner(GameObject owner) => Owner = owner;
    }
}