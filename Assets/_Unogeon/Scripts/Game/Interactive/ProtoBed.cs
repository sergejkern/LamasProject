﻿using Core.Interface;
using Core.Unity.Extensions;
using Game.Actors.Energy;
using GameUtility.Energy;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Interactive
{
    public class ProtoBed : MonoBehaviour, IInteractiveHolding
    {
        GameObject m_actor;

        public bool InteractEnabled => true;
        public void Interact(GameObject actor)
        {
            if (m_actor != null)
                return;

            //Debug.Log($"Interact {actor}");
            m_actor = actor;
            var energyCentre = m_actor.Get<EnergyCentre>();
            var bigStamina = energyCentre.GetEnergyComponent<BigStamina>();

            bigStamina.SetSleeping(true);
        }

        public void OnRelease(GameObject actor)
        {
            if (m_actor != actor)
                return;
            //Debug.Log($"OnRelease {actor}");

            var energyCentre = m_actor.Get<EnergyCentre>();
            var bigStamina = energyCentre.GetEnergyComponent<BigStamina>();
            bigStamina.SetSleeping(false);

            m_actor = null;
        }
    }
}