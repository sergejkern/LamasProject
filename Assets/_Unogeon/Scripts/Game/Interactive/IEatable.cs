﻿namespace Game.InteractiveObjects
{
    public interface IEatable : IUsable
    {
        float Energy { get; }
    }
}
