using Core.Interface;
using Game.Actors.Controls;
using GameUtility.Data.Item;
using GameUtility.Data.TransformAttachment;
using GameUtility.Interface;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public enum GrabRestrictions
    {
        None,
        PrimaryOnly,
        SecondaryOnly,
        TwoHanded,
    }

    public interface IItemProperties
    {
        GrabRestrictions GrabRestrictions { get; }
        EquipActionType ActionType { get; }

        ItemID ItemID { get; }
        Sprite Icon { get; }
        //WeaponMovesType ItemAnimationType { get; }
    }

    /// <summary>
    /// Provided Grabbable could be something else spawned by the interactive thing
    /// </summary>
    public interface IGrabProvider : 
        IProvider<IGrabbable>, IInteractive, IItemProperties
    { }

    public interface IGrabbable : IInteractive, IItemProperties
    {
        void OnGrabbed(IControlCentre grabbingActor);
        void AdjustPositioning(TransformData grabbingHand);

        void Drop();
        void Throw();
    }

    public interface IUsable
    {
        void Use(GameObject actor);
    }
}
