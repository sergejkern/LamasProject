﻿using GameUtility.Interface;
using ScriptableUtility;
using ScriptableUtility.Behaviour;
using ScriptableUtility.Graph;
using UnityEngine;

namespace Game
{
    public class InteractiveTrigger : MonoBehaviour, IInteractivePress
    {
        [SerializeField] VarReference<GameObject> m_actor;
        [SerializeField] VarReference<bool> m_interactEnabled;
        [SerializeField] ActionBehaviour m_behaviour;
        [SerializeField] ActionNameID m_id;

        public bool InteractEnabled => m_interactEnabled.Value;

        public void Interact(GameObject actor)
        {
            //Debug.Log("interact");
            m_actor.Value = actor;
            m_behaviour.InvokeAction(m_id);
        }
    }
}
