﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Game.ID;
//using UnityEngine;
//using UnityEngine.Serialization;
//namespace Game.Dialogue
//{
//    [CreateAssetMenu(menuName = "Game/DialogueSequence")]
//    public class DialogueSequence : ScriptableObject
//    {
//        // ReSharper restore UnusedMember.Global
//        [Serializable]
//        public struct DialogueElement : IDialogueElement
//        {
//            [FormerlySerializedAs("Expression")]
//            [SerializeField] ExpressionID m_expression;
//            [FormerlySerializedAs("Text")]
//            [SerializeField] string m_text;
//            [FormerlySerializedAs("Delay")]
//            [SerializeField] float m_delay;
//            [FormerlySerializedAs("ClearAfterSeconds")]
//            [SerializeField] float m_clearAfterSeconds;
//            public ExpressionID Expression => m_expression;
//            public string Text => m_text;
//            public float Delay => m_delay;
//            public float ClearAfterSeconds => m_clearAfterSeconds;
//        }
//#pragma warning disable 0649 // wrong warnings for SerializeField
//        [SerializeField] [FormerlySerializedAs("DialogueElements")]
//        DialogueElement[] m_dialogueElements;
//#pragma warning restore 0649 // wrong warnings for SerializeField
//        public IEnumerable<IDialogueElement> DialogueElements => m_dialogueElements.Cast<IDialogueElement>();
//    }
//}