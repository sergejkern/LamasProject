﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Game.Globals;
using Game.ID;
using GameUtility.Interface;
using GameUtility.Operations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Dialogue
{
    public class DialogueFlow : Singleton<DialogueFlow>
    {
        DialogueConfig m_dialogueConfig;

        public bool DialogueIsRunning => !m_coroutines.IsNullOrEmpty();
        public string Text { get; private set; }
        public string SpeakerName => m_speaker.Name;

        bool m_continue;
        bool m_speedUp;

        InputAction m_speedUpAction;
        InputAction m_okAction;
        InputAction m_upAction;
        InputAction m_downAction;

        GameObject m_speakerGO;
        GameObject m_initiatorGo;

        ICharacter m_speaker;
        IExpressionReceiver m_expressionReceiver;

        ControlBlocker DialogueBlocker => new ControlBlocker() { Source = this };

        IVar<int> m_activeChoiceIdx;
        public List<ChoiceData> Choices { get; } = new List<ChoiceData>();
        public bool HasChoices => !Choices.IsNullOrEmpty();
        public bool ChoicesActive { get; private set; }
        public int ActiveChoiceIdx
        {
            get
            {
                if (m_activeChoiceIdx == null)
                    return -1;
                return m_activeChoiceIdx.Value;
            }
        }

        readonly List<Coroutine> m_coroutines = new List<Coroutine>();

        public void ShowDialogue(DialogueConfig dialogueConfig,
            GameObject initiator,
            GameObject speaker, 
            IDialogueElement element, 
            ChoicesData choices = default)
        {
            Init(dialogueConfig, initiator, speaker, choices);
            var runner = GameGlobals.I.CoroutineRunner;

            m_coroutines.Add(runner.StartCoroutine(DialogueRoutine(element, true)));
        }

        public void ShowDialogue(DialogueConfig dialogueConfig,
            GameObject initiator,
            GameObject speaker,
            IDialogueElement[] elements, 
            ChoicesData choices = default)
        {            
            Init(dialogueConfig, initiator, speaker, choices);
            var runner = GameGlobals.I.CoroutineRunner;

            m_coroutines.Add(runner.StartCoroutine(DialogueRoutine(elements)));
        }

        void Init(DialogueConfig dialogueConfig, GameObject initiator, GameObject speaker, 
            ChoicesData choices = default)
        {
            FinishDialogue();

            m_initiatorGo = initiator;
            m_speakerGO = speaker;
            m_dialogueConfig = dialogueConfig;
            Text = "";

            speaker.TryGet(out m_speaker);
            speaker.TryGet(out m_expressionReceiver);
            GetActions(initiator, dialogueConfig, 
                out m_speedUpAction, out m_okAction, out m_upAction, out m_downAction);

            m_speedUpAction.Add(SpeedUpDialogue);
            m_okAction.Add(ContinueDialogue);

            if (choices.ChoiceIdx != null && !choices.Choices.IsNullOrEmpty())
            {
                Debug.Log("Choices!");
                Choices.AddRange(choices.Choices);
                m_activeChoiceIdx = choices.ChoiceIdx;
                m_upAction.Add(ChoiceUp);
                m_downAction.Add(ChoiceDown);
            }
        }


        OperationResult GetActions(GameObject initiator, DialogueConfig config, 
            out InputAction speedUpAction, out InputAction okAction, 
            out InputAction upAction, out InputAction downAction)
        {
            speedUpAction = null;
            okAction = null;
            upAction = null;
            downAction = null;

            if (!initiator.TryGetComponent(out IControllable controllable))
                return OperationResult.Error;

            if (config.BlockControls)
                controllable.AddControlBlocker(DialogueBlocker);

            var actions = controllable.Input.actions;
            config.GetInputActionRefs(out var speedUp, out var ok, 
                out var up, out var down);

            speedUpAction = actions[speedUp.action.name];
            okAction = actions[ok.action.name];
            upAction = actions[up.action.name];
            downAction = actions[down.action.name];

            return OperationResult.OK;
        }

        void ChoiceUp(InputAction.CallbackContext cc)
        {
            if (!ChoicesActive)
                return;
            m_activeChoiceIdx.Value = Mathf.Clamp(m_activeChoiceIdx.Value - 1, 0, Choices.Count-1);
        }

        void ChoiceDown(InputAction.CallbackContext cc)
        {
            if (!ChoicesActive)
                return;
            m_activeChoiceIdx.Value = Mathf.Clamp(m_activeChoiceIdx.Value + 1, 0, Choices.Count-1);
        }

        void SpeedUpDialogue(InputAction.CallbackContext obj) => m_speedUp = obj.ReadValue<float>() > float.Epsilon;
        void ContinueDialogue(InputAction.CallbackContext obj) => m_continue = true;

        IEnumerator DialogueRoutine(IDialogueElement[] elements)
        {
            var runner = GameGlobals.I.CoroutineRunner;

            var lastEl= elements.Last();
            foreach (var e in elements)
            {
                var coroutine = runner.StartCoroutine(DialogueRoutine(e, lastEl==e));
                m_coroutines.Add(coroutine);
                yield return coroutine;
            }
        }

        IEnumerator DialogueRoutine(IDialogueElement element, bool last)
        {
            var runner = GameGlobals.I.CoroutineRunner;
            var auto = element as IAutoDialogueElement;
            if (auto != null)
                yield return new WaitForSeconds(auto.Delay);

            m_expressionReceiver?.SetExpression(element.Expression);
            var coroutine = runner.StartCoroutine(ShowText(element));
            m_coroutines.Add(coroutine);
            yield return coroutine;

            m_continue = false;
            if (auto != null)
                yield return ClearTextAfterSeconds(auto.ClearAfterSeconds);
            else 
                yield return WaitForInput(last);

            if (last)
                FinishDialogue();
        }

        IEnumerator ShowText(IDialogueElement element)
        {
            var textLength = element.Text.Length;
            Text = "";

            for (var i = 0; i < textLength; i++)
            {
                var c = element.Text[i];
                Text += c;
                m_dialogueConfig.CharacterAudio?.Play(m_speakerGO);

                yield return new WaitForSeconds(m_speedUp 
                    ? m_dialogueConfig.TimeBetweenCharactersFast 
                    : m_dialogueConfig.TimeBetweenCharactersSlow);
            }

            m_expressionReceiver?.SetExpression(m_dialogueConfig.DefaultExpressionID);
        }

        void FinishDialogue()
        {
            m_speedUpAction?.Remove(SpeedUpDialogue);
            m_okAction?.Remove(ContinueDialogue);

            var runner = GameGlobals.I.CoroutineRunner;
            //runner.StopAllCoroutines();

            foreach (var c in m_coroutines)
            {
                if (c != null)
                    runner.StopCoroutine(c);
            }
            m_coroutines.Clear();

            ChoicesActive = false;

            if (HasChoices)
            {
                Choices.Clear();
                m_activeChoiceIdx = null;

                m_upAction?.Remove(ChoiceUp);
                m_downAction?.Remove(ChoiceDown);
            }

            if (m_initiatorGo == null) 
                return;
            if (m_dialogueConfig.BlockControls && m_initiatorGo.TryGetComponent(out IControllable controllable))
                controllable.RemoveControlBlocker(DialogueBlocker);
        }

        IEnumerator WaitForInput(bool last)
        {
            if (HasChoices && last) 
                ChoicesActive = true;
            while (!m_continue)
                yield return null;

            m_continue = false;
        }

        IEnumerator ClearTextAfterSeconds(float sec)
        {
            yield return new WaitForSeconds(sec);
            Text = "";
        }
    }

    public interface ICharacter
    {
        string Name { get; }
        //public Sprite Face
    }

    public interface IDialogueElement
    {
        ExpressionID Expression { get; }
        string Text { get; }
        // float Delay { get; }
        // float ClearAfterSeconds { get; }
    }

    public interface IAutoDialogueElement : IDialogueElement
    {
        float Delay { get; }
        float ClearAfterSeconds { get; }
    }
    public interface IExpressionReceiver
    {
        void SetExpression(ExpressionID expressionID);
    }

    public struct ChoicesData
    {
        public IEnumerable<ChoiceData> Choices;
        public IVar<int> ChoiceIdx;
    }

    [Serializable]
    public struct ChoiceData
    {
        public string ChoiceTxt;
    }
}
