﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.ID;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Dialogue
{
    [Serializable]
    public struct DialogueElement : IDialogueElement
    {
        [SerializeField] ExpressionID m_expression;
        [SerializeField] string m_text;

        public ExpressionID Expression => m_expression;
        public string Text => m_text;
    }

    [Serializable]
    public struct AutoDialogueElement : IAutoDialogueElement
    {
        [SerializeField] ExpressionID m_expression;
        [SerializeField] string m_text;
        [SerializeField] float m_delay;
        [SerializeField] float m_clearAfterSeconds;

        public ExpressionID Expression => m_expression;
        public string Text => m_text;

        public float Delay => m_delay;
        public float ClearAfterSeconds => m_clearAfterSeconds;
    }
}