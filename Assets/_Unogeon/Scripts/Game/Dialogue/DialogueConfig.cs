﻿using Core.Unity.Interface;
using Game.ID;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Dialogue
{
    [CreateAssetMenu(fileName = "DialogueConfig", menuName = "Game/DialogueConfig")]
    public class DialogueConfig : ScriptableObject
    {
        [SerializeField] float m_timeBetweenCharactersSlow;
        [SerializeField] float m_timeBetweenCharactersFast;

        [SerializeField] RefIAudioAsset m_characterAudio;
        [SerializeField] ExpressionID m_defaultExpression;

        [SerializeField] InputActionReference m_speedUpInput;
        [SerializeField] InputActionReference m_okInput;

        [SerializeField] InputActionReference m_upInput;
        [SerializeField] InputActionReference m_downInput;

        [SerializeField] bool m_blockControls = true;

        public float TimeBetweenCharactersSlow => m_timeBetweenCharactersSlow;
        public float TimeBetweenCharactersFast => m_timeBetweenCharactersFast;

        public IAudioAsset CharacterAudio => m_characterAudio.Result;
        public ExpressionID DefaultExpressionID => m_defaultExpression;

        public bool BlockControls => m_blockControls;
        
        public void GetInputActionRefs(out InputActionReference speed, out InputActionReference ok,
            out InputActionReference up, out InputActionReference down)
        {
            speed = m_speedUpInput;
            ok = m_okInput;
            up = m_upInput;
            down = m_downInput;
        }
    }
}
