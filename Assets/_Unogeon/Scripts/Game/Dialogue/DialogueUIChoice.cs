﻿using TMPro;
using UnityEngine;

namespace Game.UI
{
    public class DialogueUIChoice : MonoBehaviour
    {
        [SerializeField] TMP_Text m_choiceTxt;

        [SerializeField] Color m_activeColor;
        [SerializeField] Color m_inactiveColor;

        public bool Active;
        public void SetText(string choiceTxt) => m_choiceTxt.text = choiceTxt;

        public void SetActiveChoice(bool active)
        {
            Active = active;
            m_choiceTxt.color = active ? m_activeColor : m_inactiveColor;
        }
    }
}
