﻿using Core.Unity.Extensions;
using Core.Unity.Types;
using Game.Dialogue;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class DialogueUI : MonoBehaviour
    {
        //public static DialogueUI Instance;

        [SerializeField] TMP_Text m_characterName;

        [SerializeField] 
        TMP_Text m_dialogueText;
        [SerializeField] Animator m_animator;
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_startingAnim;
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_endingAnim;

        [SerializeField] LayoutGroup m_choicesGroup;
        [SerializeField] GameObject m_choiceElementPrefab;

        GameObject m_choicesGroupGo;
        bool m_isShown;

        int m_activeChoiceIdx;

        void Awake()
        {
            //Instance = this;
            m_choicesGroupGo = m_choicesGroup.gameObject;
        }

        void Update()
        {
            var shouldShow = DialogueFlow.I.DialogueIsRunning;
            if (shouldShow != m_isShown)
                UpdateShowing();

            if (!m_isShown)
                return;

            if (!string.Equals(m_characterName.text, DialogueFlow.I.SpeakerName))
                m_characterName.text = DialogueFlow.I.SpeakerName;
            if (!string.Equals(m_dialogueText.text, DialogueFlow.I.Text))
                m_dialogueText.text = DialogueFlow.I.Text;
            if (m_choicesGroupGo.activeSelf != DialogueFlow.I.ChoicesActive 
                || m_activeChoiceIdx != DialogueFlow.I.ActiveChoiceIdx)
                UpdateChoices();
        }

        void UpdateChoices()
        {
            Debug.Log($"UpdateChoices {DialogueFlow.I.ChoicesActive}");
            m_activeChoiceIdx = DialogueFlow.I.ActiveChoiceIdx;
            m_choicesGroupGo.SetActive(DialogueFlow.I.ChoicesActive);

            var childCount = m_choicesGroupGo.transform.childCount;
            var choices = DialogueFlow.I.Choices;
            var max = Mathf.Max(choices.Count, childCount);
            for (var i = 0; i < max; i++)
            {
                var childExists = (i < childCount);
                var childGo = childExists 
                    ? m_choicesGroupGo.transform.GetChild(i).gameObject 
                    : Instantiate(m_choiceElementPrefab, m_choicesGroupGo.transform);

                childGo.SetActive(i < choices.Count);

                if (i >= choices.Count 
                    || !childGo.TryGet<DialogueUIChoice>(out var element))
                    continue;

                element.SetText(choices[i].ChoiceTxt);
                element.SetActiveChoice(m_activeChoiceIdx == i);
            }
        }

        void UpdateShowing()
        {
            var run = DialogueFlow.I.DialogueIsRunning;
            
            m_animator.Play(run 
                ? m_startingAnim.StateHash 
                : m_endingAnim.StateHash);
            m_isShown = run;
        }
    }
}
