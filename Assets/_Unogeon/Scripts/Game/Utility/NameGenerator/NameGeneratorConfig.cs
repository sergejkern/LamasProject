﻿using System.Text;
using Core.Unity.Attributes;
using UnityEngine;

namespace Game.Utility.NameGenerator
{
    [CreateAssetMenu(fileName = "NameGeneratorConfig", menuName = "Game/NameGeneratorConfig")]
    public class NameGeneratorConfig : ScriptableObject
    {
        [SerializeField] string[] m_shortSyllables;
        [SerializeField] string[] m_midSyllables;
        [SerializeField] string[] m_lastPart;

        [SerializeField] int m_minSyllables;
        [SerializeField] int m_maxSyllables;

        [SerializeField] float m_shortSyllableRatio;
        [SerializeField] float m_lastPartChance;

        readonly StringBuilder m_stringBuilder = new StringBuilder("");

        [InspectorButton(nameof(TestName), false)] [SerializeField]
        string m_testName;

        public string GetRandomName() => GetRandomName((int) (Random.value * int.MaxValue));

        public string GetRandomName(int seed)
        {
            Random.InitState(seed);

            m_stringBuilder.Clear();
            var syllables = Random.Range(m_minSyllables, m_maxSyllables);
            for (var i = 0; i < syllables; i++)
            {
                var pickShort = Random.value < m_shortSyllableRatio;
                var pickFrom = pickShort ? m_shortSyllables : m_midSyllables;
                var part = pickFrom[Random.Range(0, pickFrom.Length)];
                m_stringBuilder.Append(part);
            }

            if (Random.value < m_lastPartChance)
            {
                var part = m_lastPart[Random.Range(0, m_lastPart.Length)];
                m_stringBuilder.Append(part);
            }

            return m_stringBuilder.ToString();
        }

        public void TestName() => m_testName = GetRandomName();
    }
}
