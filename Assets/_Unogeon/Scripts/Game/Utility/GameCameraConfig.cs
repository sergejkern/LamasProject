#pragma warning disable 0649 // wrong warnings for SerializeField

using System;
using UnityEngine;

namespace Game.Utility
{
    [CreateAssetMenu(menuName = "Game/CameraConfig")]
    public class GameCameraConfig : ScriptableObject
    {
        [SerializeField] CameraLerpSpeeds m_lerpSpeeds = new CameraLerpSpeeds()
        {
            FocusPos = 10f,
            Distance = 1f,
            Rotation = 20f,
        };

        [SerializeField] CameraSettings m_settings;

        // ReSharper disable ConvertToAutoPropertyWhenPossible
        public CameraSettings Settings => m_settings;
        public CameraLerpSpeeds LerpValues => m_lerpSpeeds;
        // ReSharper restore ConvertToAutoPropertyWhenPossible
    }

    [Serializable]
    public struct AnimatedCameraSettings
    {
        [SerializeField] internal float m_lerpTime;
        [SerializeField] internal CameraSettings m_settings;
    }

    [Serializable]
    public struct CameraSettings
    {
        //[Range(45f, 90f)]
        //[SerializeField] float m_xAngle = 65f;
        // todo 3: Ranged-Sliders:
        // ^- maybe try cinemachine?
        [SerializeField] internal Vector2 m_targetDistanceThresholds;

        [SerializeField] internal Vector2 m_cameraAngle;
        [SerializeField] internal Vector2 m_cameraDistance;
        [SerializeField] internal AnimationCurve m_cameraOffset;
    }

    [Serializable]
    public struct CameraLerpSpeeds
    {
        public float FocusPos;
        public float Distance;
        public float Rotation;

        public static CameraLerpSpeeds SetupSpeed => new CameraLerpSpeeds()
        {
            Distance = 1f,
            FocusPos = 1f,
            Rotation = 1f
        };
    }
}
