﻿using UnityEngine.Serialization;

namespace Game.Inventory
{
    [System.Serializable]
    public struct ItemStack
    {
        [FormerlySerializedAs("ItemType")] [FormerlySerializedAs("itemType")] 
        public ItemConfig ItemConfig;
        [FormerlySerializedAs("itemCount")] 
        public int ItemCount;
    }
}