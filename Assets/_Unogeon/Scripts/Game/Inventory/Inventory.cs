﻿using System.Collections.Generic;
using Game.Globals;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Inventory
{
    public class Inventory : MonoBehaviour, IInteractivePress
    {
        [FormerlySerializedAs("maxStackCount")]
        public int MaxStackCount;
        [FormerlySerializedAs("stacks")]
        public List<ItemStack> Stacks;

        GameObject m_user;

        void Start()
        {
            if (Stacks.Count > MaxStackCount)
                Debug.LogWarning(this + " contains " + Stacks.Count + " stacks, but may only contain up to " +
                                 MaxStackCount + " stacks.");
        }
	
        public bool AddStack(ItemStack stack)
        {
            if (Stacks.Count >= MaxStackCount)
                return false;

            Stacks.Add(stack);
            return true;
        }
	
        public bool AddStack(ItemConfig itemConfig, int itemCount)
        {
            ItemStack stack;
            stack.ItemConfig = itemConfig;
            stack.ItemCount = itemCount;
		
            return AddStack(stack);
        }
	
        public bool RemoveStackAt(int index)
        {
            if (index < 0 || Stacks.Count <= index)
                return false;

            Stacks.RemoveAt(index);
            return true;
        }
	
        public bool InteractEnabled => !GameGlobals.I.InventoryWindow.IsOpen();

        public void Interact(GameObject actor)
        {
            m_user = actor;
            actor.TryGetComponent<IControllable>(out var controllable);
            controllable.Input.SwitchCurrentActionMap("Menu");
            GameGlobals.I.InventoryWindow.Open(this);
        }

        public void Close()
        {
            m_user.TryGetComponent<IControllable>(out var controllable);
            controllable.Input.SwitchCurrentActionMap("Player");
        }
    }


}
