﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Inventory
{
    public class InventoryWindow : MonoBehaviour
    {
        [FormerlySerializedAs("content")]
        public GameObject Content;
        [FormerlySerializedAs("closeButtonGO")]
        public GameObject CloseButtonGO;
        [FormerlySerializedAs("itemUIPrefab")]
        public GameObject ItemUIPrefab;
        [FormerlySerializedAs("titleText")]
        public Text TitleText;

        bool m_open;
        readonly List<GameObject> m_itemUIElementGOs = new List<GameObject>();
        Inventory m_currentInventory;

        void Start()
        {
            var btn = CloseButtonGO.GetComponent<Button>();
            btn.onClick.AddListener(OnClose);
        }

        public void Initialize(Inventory inventory)
        {
            TitleText.text = inventory.gameObject.name;

            foreach (var stack in inventory.Stacks)
            {
                var newItemUIElement = Instantiate(ItemUIPrefab, Content.transform, true);
                newItemUIElement.GetComponent<Image>().sprite = stack.ItemConfig.Icon;
                m_itemUIElementGOs.Add(newItemUIElement);
            }
        }

        void CleanUp()
        {
            foreach (var itemUIElement in m_itemUIElementGOs) 
                Destroy(itemUIElement);
            m_itemUIElementGOs.Clear();
        }

        public void Open(Inventory inventory)
        {
            Initialize(inventory);
            gameObject.SetActive(true);
            m_open = true;
            m_currentInventory = inventory;
            EventSystem.current.SetSelectedGameObject(CloseButtonGO);
        }

        void OnClose()
        {
            CleanUp();
            gameObject.SetActive(false);
            m_open = false;
            m_currentInventory.Close();
        }

        public bool IsOpen() => m_open;
    }
}
