﻿using Game.InteractiveObjects;
using GameUtility.Data.Item;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Inventory
{
    // SK: renamed from ItemType to ItemConfig,
    // Config because it is a ScriptableObject,
    // I think Item-Type would be something like weapon, tool, I currently have EquipActionType
    // maybe we can use/rename this. (action controls are enabled when item in hand is of same EquipActionType)
    [CreateAssetMenu(fileName = "ItemConfig", menuName = "Game/Inventory/ItemConfig")]
    public class ItemConfig : ScriptableObject, IItemProperties
    {
        [FormerlySerializedAs("itemName")] 
        public string ItemName;
        // SK: I am thinking whether this depends on ItemType and each Item-Type has the same stack-Size
        // Currently there is EquipActionType, merge/rename with ItemType? Or do we need both?
        [FormerlySerializedAs("maxStackSize")] 
        public int MaxStackSize;
	
        // SK: would not create extra configs yet (if things grow too big then we can separate) 
        // another way to separate is to use a serialized struct,
        // configs maybe only if data is shared and user has to pick/ drag & drop 
        //[FormerlySerializedAs("inventoryItemSettings")] 
        //public InventoryItemSettings InventoryItemSettings;
        //[FormerlySerializedAs("pickupItemSettings")] 
        //public PickupItemSettings PickupItemSettings;
        //[FormerlySerializedAs("toolItemSettings")] 
        //public ToolItemSettings ToolItemSettings;
        // ---

        // SK: I changed from string to ItemID, although maybe we don't need it at all,
        // ItemConfig is already unique and we can use it to identify?
        [FormerlySerializedAs("itemId")] 
        [SerializeField] ItemID m_itemID;
        [SerializeField] Sprite m_icon;
        [SerializeField] GameObject m_prefab;

        [SerializeField] EquipActionType m_actionType;
        [SerializeField] GrabRestrictions m_grabRestrictions;

        public EquipActionType ActionType => m_actionType;
        public GrabRestrictions GrabRestrictions => m_grabRestrictions;
        public ItemID ItemID => m_itemID;
        public Sprite Icon => m_icon;
        public GameObject Prefab => m_prefab;
    }
}