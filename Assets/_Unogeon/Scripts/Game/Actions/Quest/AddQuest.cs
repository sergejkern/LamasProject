﻿using System;
using GameUtility.Interface;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    public class AddQuest : ScriptableBaseAction
    {
        [SerializeField] RefIQuest m_config;

        public override string Name => nameof(AddQuest);
        public static Type StaticFactoryType => typeof(AddQuestAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new AddQuestAction(m_config.Result);
    }


    public class AddQuestAction : IDefaultAction
    {
        readonly IQuest m_quest;
        public AddQuestAction(IQuest config) => m_quest = config;

        public void Invoke() => QuestTracker.I.AddQuest(m_quest);
    }
}