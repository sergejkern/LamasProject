﻿using System;
using System.Collections.Generic;
using GameUtility.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    public class QuestIsProgressComplete : ScriptableBaseAction, ILinkActionConfigs
    {
        [SerializeField] RefIQuest m_config;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_onYes;
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_onNo;

        public override string Name => nameof(QuestIsProgressComplete);
        public static Type StaticFactoryType => typeof(QuestIsProgressCompleteAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new QuestIsProgressCompleteAction(m_config.Result);

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_onYes, m_onNo };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_onYes, m_onNo };

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is QuestIsProgressCompleteAction canFinishQuestAction)) 
                return;
            graphActions.TryGetValue(m_onYes, out var onYesAction);
            graphActions.TryGetValue(m_onNo, out var onNoAction);

            canFinishQuestAction.LinkActions(onYesAction, onNoAction);
        }
    }


    public class QuestIsProgressCompleteAction : IUpdatingAction
    {
        readonly IQuest m_quest;
        IBaseAction m_onYes;
        IBaseAction m_onNo;

        bool m_canFinish;
        public QuestIsProgressCompleteAction(IQuest config) => m_quest = config;

        public void LinkActions(IBaseAction onYesAction, IBaseAction onNoAction)
        {
            m_onYes = onYesAction;
            m_onNo = onNoAction;
        }

        public bool IsFinished
        {
            get
            {
                var action = GetAction();
                if (action is IUpdatingAction up)
                    return up.IsFinished;
                return true;
            }
        }

        public void Init()
        {
            if (!(m_quest is IProgressQuest progress))
            {
                Debug.LogError("Quest need to implement IProgressQuest");
                return;
            }

            m_canFinish = progress.MissionProgress >= 1.0f;
            var action = GetAction();
            if (action is IUpdatingAction up)
                up.Init();
            else if (action is IDefaultAction def)
                def.Invoke();
        }

        public void Update(float dt)
        {
            if (GetAction() is IUpdatingAction up)
                up.Update(dt);
        }

        IBaseAction GetAction()
        {
            switch (m_canFinish)
            {
                case true: return m_onYes;
                case false: return m_onNo;
            }
            return null;
        }
    }
}