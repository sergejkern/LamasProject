﻿using System;
using GameUtility.Interface;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    public class FinishQuest : ScriptableBaseAction
    {
        [SerializeField] RefIQuest m_config;

        public override string Name => nameof(FinishQuest);
        public static Type StaticFactoryType => typeof(FinishQuestAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new FinishQuestAction(m_config.Result);
    }


    public class FinishQuestAction : IDefaultAction
    {
        readonly IQuest m_quest;
        public FinishQuestAction(IQuest config) => m_quest = config;

        public void Invoke() => QuestTracker.I.FinishQuest(m_quest);
    }
}