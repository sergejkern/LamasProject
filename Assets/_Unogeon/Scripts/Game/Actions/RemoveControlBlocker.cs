﻿using System;
using Core.Interface;
using GameUtility.Interface;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    public class RemoveControlBlocker : ScriptableBaseAction
    {
        [SerializeField, Input] VarReference<GameObject> m_actor;

        public override string Name => nameof(RemoveControlBlocker);
        public static Type StaticFactoryType => typeof(RemoveControlBlockerAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() 
            => new RemoveControlBlockerAction(m_actor.Init(nameof(m_actor), Node), m_actor.Context);
    }


    public class RemoveControlBlockerAction : IDefaultAction
    {
        readonly IVar<GameObject> m_actor;
        readonly object m_blocker;
        public RemoveControlBlockerAction(IVar<GameObject> actor, object blocker)
        {
            m_actor = actor;
            m_blocker = blocker;
        }

        public void Invoke()
        {
            if (!m_actor.Value.TryGetComponent(out IControllable controllable))
               return;

            controllable.RemoveControlBlocker(new ControlBlocker(){ Source = m_blocker });
        }
    }
}