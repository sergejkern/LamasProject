﻿using System;
using Core.Interface;
using GameUtility.Interface;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    public class AddControlBlocker : ScriptableBaseAction
    {
        [SerializeField, Input] VarReference<GameObject> m_actor;

        public override string Name => nameof(AddControlBlocker);
        public static Type StaticFactoryType => typeof(AddControlBlockerAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() 
            => new AddControlBlockerAction(m_actor.Init(nameof(m_actor), Node), m_actor.Context);
    }


    public class AddControlBlockerAction : IDefaultAction
    {
        readonly IVar<GameObject> m_actor;
        readonly object m_blocker;
        public AddControlBlockerAction(IVar<GameObject> actor, object blocker)
        {
            m_actor = actor;
            m_blocker = blocker;
        }

        public void Invoke()
        {
            if (!m_actor.Value.TryGetComponent(out IControllable controllable))
               return;

            controllable.AddControlBlocker(new ControlBlocker(){ Source = m_blocker });
        }
    }
}