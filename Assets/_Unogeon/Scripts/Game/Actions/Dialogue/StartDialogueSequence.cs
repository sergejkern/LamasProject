﻿//using System;
//using Core.Interface;
//using Game.Configs;
//using Game.Dialogue;
//using nodegraph.Attributes;
//using ScriptableUtility;
//using ScriptableUtility.ActionConfigs;
//using UnityEngine;
//using UnityEngine.InputSystem;
//namespace Game.Actions
//{
//    public class StartDialogueSequence : ScriptableBaseAction
//    {
//        [SerializeField, Input] VarReference<GameObject> m_dialogueInitiator;
//        //[SerializeField] DialogueUI m_ui;
//        [SerializeField] DialogueSequence m_sequence;
//        [SerializeField] InputActionReference m_inputActionReference;
//        public override string Name => nameof(StartDialogueSequence);
//        public static Type StaticFactoryType => typeof(StartDialogueSequenceAction);
//        public override Type FactoryType => StaticFactoryType;
//        public override IBaseAction CreateAction()
//        {
//            return new StartDialogueSequenceAction(
//                m_dialogueInitiator.Init(nameof(m_dialogueInitiator), Node), 
//                //m_ui,
//                m_sequence, m_inputActionReference);
//        }
//    }
//    public class StartDialogueSequenceAction : IDefaultAction
//    {
//        public StartDialogueSequenceAction(IVar<GameObject> dialogueInitiator, 
//            //DialogueUI dialogueUI,
//            DialogueSequence dialogueSequence, InputActionReference actionReference)
//        {
//            m_dialogueInitiator = dialogueInitiator;
//            //m_dialogue = dialogueUI;
//            m_sequence = dialogueSequence;
//            m_actionReference = actionReference;
//        }
//        readonly IVar<GameObject> m_dialogueInitiator;
//        //readonly DialogueUI m_dialogue;
//        readonly DialogueSequence m_sequence;
//        readonly InputActionReference m_actionReference;
//        public void Invoke()
//        {
//            var initiator = m_dialogueInitiator.Value;
//            if (initiator == null)
//                return;
//            var dialogueConfig = ConfigLinks.Instance.GetDialogueConfig();
//            DialogueFlow.I.ShowDialogue(dialogueConfig, initiator, null, m_sequence.DialogueElements);
//        }
//    }
//}