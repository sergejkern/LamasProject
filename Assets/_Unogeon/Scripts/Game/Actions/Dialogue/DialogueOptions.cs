﻿using System;
using Game.Dialogue;
using Game.UI;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;

namespace Game.Actions
{
    public class DialogueOptions : ScriptableBaseAction
    {
        public override string Name => nameof(DialogueOptions);
        public static Type StaticFactoryType => typeof(DialogueOptionsAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new DialogueOptionsAction();
    }


    public class DialogueOptionsAction : IUpdatingAction
    {
        public bool IsFinished => !DialogueFlow.I.DialogueIsRunning;
        public void Init() { }

        public void Update(float dt) { }
    }
}