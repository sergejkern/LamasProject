﻿using System;
using System.Linq;
using Core.Interface;
using Game.Configs;
using Game.Dialogue;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    [NodeIcon("dialogue.png")]
    public class ShowDialogue : ScriptableBaseAction
    {
        [SerializeField, Input] VarReference<GameObject> m_speaker;
        [SerializeField, Input] VarReference<GameObject> m_dialogueInitiator;
        [SerializeField] DialogueElement[] m_dialogueElements;

        public override string Name => nameof(ShowDialogue);
        public static Type StaticFactoryType => typeof(ShowDialogueAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new ShowDialogueAction(
                m_dialogueInitiator.Init(nameof(m_dialogueInitiator), Node), 
                m_speaker.Init(nameof(m_speaker), Node),
                m_dialogueElements.Cast<IDialogueElement>().ToArray());
        }
    }


    public class ShowDialogueAction : IUpdatingAction
    {
        public ShowDialogueAction(IVar<GameObject> dialogueInitiator, 
            IVar<GameObject> speaker, 
            IDialogueElement[] dialogueElements)
        {
            m_dialogueInitiator = dialogueInitiator;
            m_speaker = speaker;

            m_dialogueElements = dialogueElements;
        }

        readonly IVar<GameObject> m_dialogueInitiator;
        readonly IVar<GameObject> m_speaker;

        readonly IDialogueElement[] m_dialogueElements;

        public bool IsFinished => !DialogueFlow.I.DialogueIsRunning;
        public void Init()
        {
            var initiator = m_dialogueInitiator.Value;
            var speaker = m_speaker.Value;

            if (speaker == null)
                return;

            var dialogueConfig = ConfigLinks.Instance.GetDialogueConfig();
            DialogueFlow.I.ShowDialogue(dialogueConfig, initiator, speaker, m_dialogueElements);
        }

        public void Update(float dt) {}
    }
}