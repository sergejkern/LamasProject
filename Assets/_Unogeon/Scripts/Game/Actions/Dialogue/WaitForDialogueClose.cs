﻿using System;
using Game.Dialogue;
using Game.UI;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;

namespace Game.Actions
{
    public class WaitForDialogueClose : ScriptableBaseAction
    {
        public override string Name => nameof(WaitForDialogueClose);
        public static Type StaticFactoryType => typeof(WaitForDialogueCloseAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new WaitForDialogueCloseAction();
    }


    public class WaitForDialogueCloseAction : IUpdatingAction
    {
        public bool IsFinished => !DialogueFlow.I.DialogueIsRunning;
        public void Init() { }

        public void Update(float dt) { }
    }
}