﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Game.Configs;
using Game.Dialogue;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Game.Actions
{
    [NodeIcon("dialogue.png")]
    public class ShowDialogueWithChoices : ScriptableBaseAction, ILinkActionConfigs
    {
        [Serializable]
        public struct ChoiceActionData
        {
            public string Text;
            public ScriptableBaseAction Action;
        }

        [SerializeField, Input] VarReference<GameObject> m_speaker;
        [SerializeField, Input] VarReference<GameObject> m_dialogueInitiator;
        [SerializeField, Input] VarReference<int> m_activeChoice;

        [SerializeField] DialogueElement[] m_dialogueElements;

        [NodeConnectorList(IO.Output, typeof(ScriptableActionConnector), 
            nameof(ChoiceActionData.Text), nameof(ChoiceActionData.Action))]
        [SerializeField] ChoiceActionData[] m_choiceActionData;

        public override string Name => nameof(ShowDialogueWithChoices);
        public static Type StaticFactoryType => typeof(ShowDialogueChoicesAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var choices = new ChoiceData[m_choiceActionData.Length];
            for (var i = 0; i < choices.Length; i++)
                choices[i].ChoiceTxt = m_choiceActionData[i].Text;

            return new ShowDialogueChoicesAction(
                m_dialogueInitiator.Init(nameof(m_dialogueInitiator), Node), 
                m_speaker.Init(nameof(m_speaker), Node),
                m_activeChoice.Init(nameof(m_activeChoice), Node),
                m_dialogueElements.Cast<IDialogueElement>().ToArray(),
                choices);
        }

        public IEnumerable<ScriptableObject> Dependencies => m_choiceActionData.Select(a => a.Action);
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => m_choiceActionData.Select(a => a.Action);

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is ShowDialogueChoicesAction action)) 
                return;

            var actions = new IBaseAction[m_choiceActionData.Length];
            for (var i = 0; i < m_choiceActionData.Length; i++)
            {
                if (m_choiceActionData[i].Action != null 
                    && graphActions.TryGetValue(m_choiceActionData[i].Action, out var ac))
                    actions[i] = ac as IDefaultAction;
            }   
            action.LinkActions(actions);
        }
    }


    public class ShowDialogueChoicesAction : IUpdatingAction
    {
        public ShowDialogueChoicesAction(IVar<GameObject> dialogueInitiator, 
            IVar<GameObject> speaker, IVar<int> choicesVar,
            IDialogueElement[] dialogueElements, ChoiceData[] choiceData)
        {
            m_dialogueInitiator = dialogueInitiator;
            m_speaker = speaker;
            m_choicesVar = choicesVar;

            m_dialogueElements = dialogueElements;
            m_choiceData = choiceData;
            m_selectedChoice = -1;
        }

        readonly IVar<GameObject> m_dialogueInitiator;
        readonly IVar<GameObject> m_speaker;
        readonly IVar<int> m_choicesVar;
        readonly ChoiceData[] m_choiceData;

        readonly IDialogueElement[] m_dialogueElements;
        IBaseAction[] m_actions;

        int m_selectedChoice;

        public bool IsFinished
        {
            get
            {
                if (m_selectedChoice ==-1)
                    return false;
                if (m_actions[m_selectedChoice] is IUpdatingAction up)
                    return up.IsFinished;

                return true;
            }
        }

        public void Init()
        {
            var initiator = m_dialogueInitiator.Value;
            var speaker = m_speaker.Value;
            m_selectedChoice = -1;
            if (speaker == null)
                return;

            var dialogueConfig = ConfigLinks.Instance.GetDialogueConfig();
            DialogueFlow.I.ShowDialogue(dialogueConfig, initiator, speaker, m_dialogueElements, 
                new ChoicesData()
                {
                    ChoiceIdx = m_choicesVar,
                    Choices = m_choiceData
                });
        }

        public void Update(float dt)
        {
            switch (m_selectedChoice)
            {
                case -1: UpdateWaitForChoice(); break;
                default: UpdateFollowingAction(dt); break;
            }
        }

        void UpdateFollowingAction(float dt)
        {
            Debug.Assert(m_selectedChoice.IsInRange(m_actions));
            if (m_actions[m_selectedChoice] is IUpdatingAction updatingAction)
                updatingAction.Update(dt);
        }

        void UpdateWaitForChoice()
        {
            if (DialogueFlow.I.DialogueIsRunning)
                return;

            m_selectedChoice = m_choicesVar.Value;
            Debug.Assert(m_selectedChoice.IsInRange(m_actions));
            switch (m_actions[m_selectedChoice])
            {
                case IUpdatingAction up:
                    up.Init();
                    break;
                case IDefaultAction def:
                    def.Invoke();
                    break;
            }
        }

        public void LinkActions(IBaseAction[] actions) 
            => m_actions = actions;
    }


}