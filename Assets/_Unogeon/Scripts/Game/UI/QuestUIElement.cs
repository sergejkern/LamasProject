﻿using UnityEngine;

namespace Game.UI
{
    public class QuestUIElement : MonoBehaviour
    {
        public TMPro.TMP_Text Title;
        public TMPro.TMP_Text ShortText;

        public void SetText(string title, string shortText)
        {
            Title.text = title;
            ShortText.text = shortText;
        }
    }
}
