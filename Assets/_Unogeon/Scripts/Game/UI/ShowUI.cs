﻿using System;
using ScriptableUtility;
using UnityEngine;

namespace Game.UI
{
    public class ShowUI : MonoBehaviour, IInitWithContext, IScriptableEventListener<bool>
    {
        [SerializeField] ScriptableVariable<bool> m_show;
        [SerializeField] CanvasGroup m_canvasGroup;

        [SerializeField] float m_alphaOnShow;
        [SerializeField] float m_transitionSeconds = 1f;

        float m_from;
        float m_target;
        float m_lerpValue;

        public void Init(IContext ctx)
        {
            var varRef = new VarReference<bool>(ctx, m_show);
            m_show.RegisterListener(ctx, this);

            OnEvent(varRef.Value);
        }

        void Update()
        {
            if (Math.Abs(m_canvasGroup.alpha - m_target) < float.Epsilon)
                return;
            if (m_transitionSeconds< float.Epsilon)
                return;

            m_canvasGroup.alpha = Mathf.Lerp(m_from, m_target, m_lerpValue);
            m_lerpValue += Time.unscaledDeltaTime / m_transitionSeconds;
        }

        public void OnEvent(bool show)
        {
            m_from = m_canvasGroup.alpha;
            m_lerpValue = 0f;
            m_target = show ? m_alphaOnShow : 0f;
        }
    }
}
