﻿using System;
using Core.Events;
using UnityEngine;

namespace Game.UI
{
    public class QuestUI : MonoBehaviour, IEventListener<QuestStatusChanged>
    {
        [SerializeField] GameObject m_questElementPrefab;
        [SerializeField] Transform m_contentParent;

        void OnEnable() => EventMessenger.AddListener(this);

        void OnDisable() => EventMessenger.RemoveListener(this);

        void Update()
        {
            // todo: dont do this on update
            var i = 0;
            foreach (var activeQuest in QuestTracker.I.ActiveQuests)
            {
                if (i >= m_contentParent.childCount)
                    return;
                m_contentParent.GetChild(i).TryGetComponent(out QuestUIElement element);
                element.SetText(activeQuest.Title, activeQuest.ShortText);
                ++i;
            }
        }

        public void OnEvent(QuestStatusChanged eventType)
        {
            var activeCount = QuestTracker.I.ActiveQuestCount;
            //Debug.Log($"{activeCount}");

            for (var i = 0; i < m_contentParent.childCount; i++)
                m_contentParent.GetChild(i).gameObject.SetActive(i < activeCount);
        }
    }
}
