﻿using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Types;
using Game.Globals;
using Game.Interactive;
using GameUtility.Data.Item;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Configs
{
    // todo: should we instantiate the quest from config instead of using the config itself?
    [CreateAssetMenu(fileName = nameof(GatherQuest), menuName = "Game/Quest/GatherQuest")]
    public class GatherQuest : QuestConfig, IProgressQuest, IEventListener<InventoryChanged>
    {
        [SerializeField] ItemID m_itemID;
        [SerializeField] int m_gatherAmount;

        public override string ShortText => $"{m_currentlyGathered} / {m_gatherAmount} {m_itemID.name}";
        public float MissionProgress
        {
            get
            {
                //Debug.Log($"m_gatherAmount {m_gatherAmount} m_currentlyGathered {m_currentlyGathered}");
                if (m_gatherAmount <= 0)
                    return 1f;

                return (float) m_currentlyGathered / m_gatherAmount;
            }
        }

        int m_currentlyGathered;

        public override void OnQuestStarted() => EventMessenger.AddListener(this);
        public override void OnQuestComplete() => EventMessenger.RemoveListener(this);

        int GetAmountGathered()
        {
            using (var invScope = SimplePool<List<IInventory>>.I.GetScoped())
            {
                var inventories = invScope.Obj;
                foreach (var p in PlayerBookkeeper.I.Players)
                {
                    p.Get(out IInventory inventory);
                    if (inventory != null)
                        inventories.Add(inventory);
                }

                return inventories.Sum(i => i.GetItemAmount(m_itemID));
            }
        }

        public void OnEvent(InventoryChanged eventType) => m_currentlyGathered = GetAmountGathered();
    }
}
