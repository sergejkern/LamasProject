﻿using GameUtility.Interface;
using UnityEngine;

namespace Game.Configs
{
    public abstract class QuestConfig : ScriptableObject, IQuest
    {
        [SerializeField] string m_title;
        [SerializeField] string m_description;

        [SerializeField] QuestTypeID m_questTypeID;

        public string Title => m_title;
        public string Description => m_description;
        public QuestTypeID QuestTypeID => m_questTypeID;

        public abstract string ShortText { get; }
        public abstract void OnQuestStarted();
        public abstract void OnQuestComplete();
    }
}
