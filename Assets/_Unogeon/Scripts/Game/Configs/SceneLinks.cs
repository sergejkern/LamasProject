using System;
using Core.Unity.Types;
using GameUtility.GameSetup;
using UnityEngine;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/SceneLinks")]
    public class SceneLinks : ScriptableObject
    {
        public static SceneLinks Instance { get; internal set; }
        public static SceneLinks I => Instance;

        [Serializable]
        public struct SceneLink
        {
            public SceneID ID;
            public SceneReference Scene;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] SceneLink[] m_scenes;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public SceneReference GetScene(SceneID id) => Array.Find(m_scenes, s => s.ID == id).Scene;
    }
}
