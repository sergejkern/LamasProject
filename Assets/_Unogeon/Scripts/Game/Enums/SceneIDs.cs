//! THIS FILE IS AUTOGENERATED, DO NOT MODIFY
// ReSharper disable UnusedMember.Global
namespace Game.Enums
{
	public enum SceneIDs
	{
		TitleScreen,
		GameOver,
		Pause,
		Game,
	}
}
