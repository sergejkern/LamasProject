using System.Collections.Generic;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Game.Configs;
using Game.Inventory;
using Game.Utility;
using GameUtility.GameSetup;
using GameUtility.Interface.Globals;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Game.Globals
{
    public class GameGlobals : Singleton<GameGlobals>, IGameGlobals
    {
#region Need Initialization

        internal GameSetup GameSetup;

        internal static PrefabLinks Prefabs
        {
            get => PrefabLinks.Instance;
            set => PrefabLinks.Instance = value;
        }

        public static SceneLinks SceneLinks 
        {
            get => SceneLinks.Instance;
            set => SceneLinks.Instance = value;
        }

        internal static ConfigLinks ConfigLinks
        {
            get => ConfigLinks.Instance;
            set => ConfigLinks.Instance = value;
        }

        #region GameObject Initialization-Properties
        public GameObject GOGameCamera
        {
            get => m_goGameCamera;
            set
            {
                InitGOVar_GetComponentInChildren<GameCamera>(ref m_goGameCamera, 
                    value, out var component);

                GameCamera = component;
                // I don't care who runs my coroutines
                CoroutineRunner = component;
            }
        }
        public GameObject GOSceneLights
        {
            get => m_goSceneLights;
            set
            {
                InitGOVar_GetComponent<SceneLights>(ref m_goSceneLights, 
                    value, out var component);
                SceneLights = component;
            }
        }

        public GameObject GOAudio
        {
            get => m_goAudio;
            set => InitGOVar(ref m_goAudio, value);
        }

        public GameObject GOPlayerInputManager
        {
            get => m_goPlayerInputManager;
            set
            {
                InitGOVar_GetComponent<PlayerInputManager>(ref m_goPlayerInputManager, 
                    value, out var component);
                PlayerInputManager = component;
            }
        }

        public GameObject GO_UICanvas
        {
            get => m_goUICanvas;
            set => InitGOVar(ref m_goUICanvas, value);
        }

        public GameObject GO_InventoryWindow
        {
            get => m_goInventoryWindow;
            set
            {
                InitGOVar_GetComponent<InventoryWindow>(ref m_goInventoryWindow, 
                    value, out var component);

                InventoryWindow = component;
            }
        }

        public GameObject GO_SceneGUI
        {
            get => m_goSceneGUI;
            set
            {
                InitGOVar_GetComponent<SceneGUI>(ref m_goSceneGUI, 
                    value, out var component);
                SceneGUI = component;
            }
        }
        #endregion
        #endregion

        // public bool IsTutorial;
        public MonoBehaviour CoroutineRunner { get; private set; }
        public GameCamera GameCamera { get; private set; }
        IGameCamera IGameGlobals.GameCamera => GameCamera;

        public SceneLights SceneLights { get; private set; }
        public PlayerInputManager PlayerInputManager { get; private set; }
        public IPlayerBookkeeper PlayerManager => PlayerBookkeeper.I;

        public SceneGUI SceneGUI { get; private set; }

        public InventoryWindow InventoryWindow { get; private set; }

        public SceneSettings CurrentSceneSettings => GameSetup.m_currentSceneSettings;
        ISceneSettings IGameGlobals.CurrentSceneSettings => CurrentSceneSettings;

        public Scene RuntimeScene { get; set; }


        public readonly Dictionary<SceneID, ISceneRoot> Scenes = new Dictionary<SceneID, ISceneRoot>();

        GameObject m_goAudio;
        GameObject m_goGameCamera;
        GameObject m_goSceneLights;
        GameObject m_goPlayerInputManager;
        GameObject m_goAudioManager;
        GameObject m_goUICanvas;
        GameObject m_goInventoryWindow;
        GameObject m_goSceneGUI;

        public void Reset()
        {
            if (PlayerInputManager != null)
                PlayerInputManager.playerJoinedEvent.RemoveAllListeners();

            GameSetup = null;
            Prefabs = null;
            ConfigLinks = null;

            GOGameCamera = null;
            GOSceneLights = null;
            GOAudio = null;

            GOPlayerInputManager = null;
            GO_UICanvas = null;
            GO_SceneGUI = null;
        }

        public void RemoveAllInput()
        {
            foreach (var p in PlayerManager.Players)
            {
                if (p == null)
                    continue;

                var centre = p.Centre;
                var input = centre.Input;
                p.RemoveControl();
                input.gameObject.DestroyEx();
            }
            GOPlayerInputManager.DestroyEx();
            GOPlayerInputManager = null;
        }

        public void SetScene(SceneID sceneID, ISceneRoot sceneRoot)
        {
            if (Scenes.ContainsKey(sceneID))
                Scenes[sceneID] = sceneRoot;
            else 
                Scenes.Add(sceneID, sceneRoot);
        }

        protected override void Init()
        {
            base.Init();
            GlobalAccess.GameGlobals = this;
        }

        static void InitGOVar(ref GameObject variable, GameObject value)
        {
            if (variable != null)
                variable.DestroyEx();

            variable = value;
        }

        static void InitGOVar_GetComponent<T>(ref GameObject variable, GameObject value, out T component)
        {
            component = default;
            InitGOVar(ref variable, value);

            if (variable == null)
                return;

            variable.TryGetComponent(out component);
        }

        static void InitGOVar_GetComponentInChildren<T>(ref GameObject variable, GameObject value, out T component)
        {
            component = default;
            InitGOVar(ref variable, value);
            if (variable == null)
                return;

            component = variable.GetComponentInChildren<T>();
        }
    }
}
