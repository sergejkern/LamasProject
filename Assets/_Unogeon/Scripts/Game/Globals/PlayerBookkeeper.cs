
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Game.Actors;
using GameUtility.Interface;
using GameUtility.Interface.Globals;

namespace Game.Globals
{
    public class PlayerBookkeeper : Singleton<PlayerBookkeeper>, IPlayerBookkeeper
    {
        public int PlayerCount { get; private set; }

        // can have null entries
        public List<Player> Players { get; } = new List<Player>();
        IEnumerable<IPlayer> IPlayerBookkeeper.Players => Players;

        public IPlayer GetPlayer(int i) => i.IsInRange(Players) ? Players[i] : null;

        public void SetPlayer(int i, IPlayer pl) => Players[i] = (Player)pl;

        public void AddPlayer(IPlayer pl) => Players.Add((Player)pl);

        public void UpdatePlayerCount()
        {
            var playerCount = 0;
            foreach (var player in Players)
            {
                if (player == null)
                    continue;

                player.SetPlayerIdx(playerCount);
                playerCount++;
            }

            PlayerCount = playerCount;
        }

        public void Reset() => Players.Clear();
    }
}