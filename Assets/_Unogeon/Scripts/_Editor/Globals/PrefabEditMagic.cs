﻿using Core.Unity.Types;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

namespace _Editor.Game.Globals
{
    [InitializeOnLoad]
    public static class PrefabEditMagic
    {
        public static GameObject CurrentPrefab { get; private set; }

        static PrefabEditMagic()
        {
            EditorApplication.update += Update;
        }

        static void Update()
        {
            var stage = PrefabStageUtility.GetCurrentPrefabStage();
            if (stage == null)
            {
                UpdatePrefab(null);
                return;
            }

            UpdatePrefab(stage.prefabContentsRoot);
        }

        static void UpdatePrefab(GameObject prefab)
        {
            if (CurrentPrefab == prefab)
                return;
            CurrentPrefab = prefab;
            if (CurrentPrefab != null)
                OnPrefabOpened();
        }

        static void OnPrefabOpened()
        {
            SpawnPrefabRefs();
            OpenPrefabWizard();
        }

        static void SpawnPrefabRefs()
        {
            var prefabRefs = CurrentPrefab.GetComponentsInChildren<PrefabRef>();
            foreach (var prefabRef in prefabRefs)
                prefabRef.Editor_SpawnPreview();
        }
        static void OpenPrefabWizard() => PrefabWizard.OpenWindow();
    }
}
