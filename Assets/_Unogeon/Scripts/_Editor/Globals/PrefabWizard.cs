﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using _Editor.Validation;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Events;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.GUITools;
using Game.Actors.Controls;
using Game.Interactive;
using GameUtility.Data.TransformAttachment;
using GameUtility.Editor.ConfigInspector;
using GameUtility.Energy;
using GameUtility.Interface;
using ScriptableUtility;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Editor.Game.Globals
{
    public class PrefabWizard : EditorWindow, IEventListener<PrefabVerified>
    {
        struct Module<T>
        {
            public Type Type => typeof(T);
            public string Name;
            public string Description;

            public T Instance;
            public SelectTypesPopup SelectTypesPopup;
        }

        #region Variables
        // Prefab
        PrefabStage m_currentStage;
        GameObject m_prefab;
        // -----
        // Creating Prefab:
        string m_gameObjectName;
        // -----
        // Directory
        string m_dirName;
        DefaultAsset m_dir;
        // -----
        // Prefab Modules
        Module<IPrefabArchetype> m_archetype = new Module<IPrefabArchetype>() { Name = "Archetype", Description = "At Root of the Object" +
            "This will give information about which other component-modules are needed."};
        Module<ISpawnAPrefab> m_modelSpawner = new Module<ISpawnAPrefab>() { Name = "Model Spawner", Description = "This will spawn the model for this object." +
            "You can modify offset for correct positioning."};
        Module<IContext> m_ctxComponent = new Module<IContext>() { Name = "Context", Description = "Context Component for Scriptable Utility System" +
            "You need this for Action and FSM-Graphs and easily sharing variables f.e. for UI."};
        Module<IAttachmentPoints> m_attachmentPoints = new Module<IAttachmentPoints>(){ Name = "Attachment Points", Description = "A provider for all attachment points" +
            "required by other components. eg. like HandR for grabbing items with right hand"};
        Module<IControlCentre> m_controlCentre = new Module<IControlCentre>(){ Name = "Control Centre", Description = "Control Centre maps all the Input to" +
            "ActionComponents requiring this Input. It can also memorize commands for combos, rumble controller, block/unblock controls." +
            "Action-Components should be on the same Object."};
        Module<IEnergyCentre> m_energyCentre = new Module<IEnergyCentre>(){ Name = "Energy Centre", Description = "Energy Centre is the Main Object and provider" +
            "for all Energy Types which should be on the same Object, eg. HealthComponent, Stamina, etc."};
        Module<IMutableTransformData> m_transformModule = new Module<IMutableTransformData>(){ Name = "Transform", Description = "Component for moving the object." +
            "Responsible for smooth movement and raycast if position is OK, etc."};
        Module<IEquipment> m_equipModule = new Module<IEquipment>(){ Name = "Equipment", Description = "Component for current Equipment of items. eg. left hand, right hand" };
        Module<IInventory> m_inventoryModule = new Module<IInventory>(){ Name = "Inventory", Description = "Component for putting and storing items." };
        // -----
        // Module Viewer:
        object m_viewingComponent;
        Editor m_viewingEditor;
        // -----
        // Verification
        VerificationResult m_lastResult = VerificationResult.Default;
        // -----
        // GUI
        Vector2 m_scrollPos;
        // -----
        #endregion

        #region Window-Init, Open, OnDisable
        [MenuItem("Tools/PrefabWizard &w")]
        public static void OpenWindow()
        {
            var prefabWizard = GetWindow(typeof(PrefabWizard), false, "Prefab Wizard");
            prefabWizard.Show();
        }

        void OnEnable()
        {
            EventMessenger.AddListener(this);

            InitModuleTypesPopup(ref m_archetype, (td) => AddTypeAsComponent(ref m_archetype, td));

            InitModuleTypesPopup(ref m_transformModule, (td) => AddTypeAsComponent(ref m_transformModule, td));
            InitModuleTypesPopup(ref m_ctxComponent,(td) => AddTypeAsComponent(ref m_ctxComponent, td));

            InitModuleTypesPopup(ref m_inventoryModule,(td) => AddTypeAsComponent(ref m_inventoryModule, td));
            InitModuleTypesPopup(ref m_equipModule,(td) => AddTypeAsComponent(ref m_equipModule, td));

            InitModuleTypesPopup(ref m_controlCentre,(td) => AddTypeAsChild(ref m_controlCentre, td));
            InitModuleTypesPopup(ref m_energyCentre,(td) => AddTypeAsChild(ref m_energyCentre, td));

            InitModuleTypesPopup(ref m_modelSpawner, (td) => AddTypeAsChild(ref m_modelSpawner, td));
            InitModuleTypesPopup(ref m_attachmentPoints, (td) => AddTypeAsChild(ref m_attachmentPoints, td));
        }

        void OnDisable() => EventMessenger.RemoveListener(this);
        #endregion

        #region GUI
        void OnGUI()
        {
            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scroll.scrollPosition;

                m_currentStage = PrefabStageUtility.GetCurrentPrefabStage();
                if (m_currentStage == null)
                {
                    NoPrefabStageGUI();
                    return;
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    if (m_prefab != m_currentStage.prefabContentsRoot
                        || GUILayout.Button("Update"))
                        SyncCurrentPrefab();

                    if (GUILayout.Button("Verify!"))
                        SyncAndVerify();
                }

                VerifyGUI();


                PrefabGUI();
            }
        }

        void PrefabGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_gameObjectName = EditorGUILayout.DelayedTextField("Name:", m_prefab.name);
                if (check.changed) 
                    OnRename();
            }

            GUILayout.Label($"path: {m_currentStage.assetPath}");
            GUILayout.Label($"Directory {m_dirName}");
            using (new EditorGUI.DisabledScope(true))
                EditorGUILayout.ObjectField(m_dir, typeof(DefaultAsset), true);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                ModularComponentGUI(m_archetype);

                if (m_archetype.Instance != null)
                {
                    ModularComponentGUIOption(m_ctxComponent);
                    ModularComponentGUIOption(m_transformModule);

                    ModularComponentGUIOption(m_inventoryModule);
                    ModularComponentGUIOption(m_equipModule);

                    ModularComponentGUIOption(m_controlCentre);
                    ModularComponentGUIOption(m_energyCentre);

                    ModularComponentGUIOption(m_modelSpawner);
                    ModularComponentGUIOption(m_attachmentPoints);
                }
                if (check.changed)
                    SyncAndVerify();
            }
        }

        void OnRename()
        {
            m_prefab.name = m_gameObjectName;

            // todo: folder rename
            // AssetDatabase.SaveAssets();
            // var allAssetsInDir = AssetDatabase.FindAssets("", new[] {m_dirName});
            // AssetDatabase.MoveAsset(oldPath:, newPath:);
        }

        void ViewingComponentGUI()
        {
            Editor.CreateCachedEditor(m_viewingComponent as Object, null, ref m_viewingEditor);
            if (m_viewingComponent == null)
                return;
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                if (m_viewingEditor is IIsolatedGUI wizardGUI)
                    wizardGUI.IsolatedGUI(position.width);
                else m_viewingEditor.OnInspectorGUI();
            }
        }
        #endregion
        
        #region Module Handling
        static void InitModuleTypesPopup<T>(ref Module<T> ts, GenericMenu.MenuFunction2 func)
        {
            EditorExtensions.ReflectionGetAllTypes(ts.Type, out var types, 
                out var names, out var namespaces);
            ts.SelectTypesPopup = EditorExtensions.CreateTypeSelectorPopup(types, names, namespaces, 
                EditorExtensions.MenuFromNamespace.Default, func);
        }

        void InitPrefabModule<T>(ref Module<T> ts)
        {
            ts.Instance = m_prefab.Get<T>();
            if (ts.Instance != null)
                return;
            ts.Instance = m_prefab.GetComponentInChildren<T>();
        }

        void AddTypeAsComponent<T>(ref Module<T> ts, object typeData) where T : class 
        {
            var type = typeData as Type;
            ts.Instance = m_prefab.AddComponent(type) as T;
        }
        void AddTypeAsChild<T>(ref Module<T> ts, object typeData) where T : class 
        {
            var type = typeData as Type;
            var child = new GameObject(ts.Name, type);
            child.transform.SetParent(m_prefab.transform);
            ts.Instance = child.Get<T>();
        }

        void ModularComponentGUIOption<T>(Module<T> ts) where T : class
        {
            var logic = m_archetype.Instance;
            if (logic.ModuleTypes.Contains(ts.Type))
                ModularComponentGUI(ts);
        }
        void ModularComponentGUI<T>(Module<T> ts) where T : class
        {
            if (ts.Instance == null)
            {
                EditorGUILayout.HelpBox(ts.Description, MessageType.Info);
                using (new ColorScope(Color.yellow))
                {
                    if (GUILayout.Button($"Add {ts.Name}"))
                        PopupWindow.Show(position, ts.SelectTypesPopup);
                }
            }
            else
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    using (new EditorGUILayout.HorizontalScope(GUILayout.Width(50f)))
                    {
                        var errLvl = ErrorLevel(ts);
                        if (errLvl != MessageType.None)
                            EditorGUILayout.HelpBox("=>", errLvl);
                    }


                    if (CustomGUI.ActivityButton(m_viewingComponent == ts.Instance, $"{ts.Name}", false))
                    {
                        m_viewingComponent = ts.Instance == m_viewingComponent
                            ? null
                            : ts.Instance;
                    }
                }
            }         
            if (m_viewingComponent == ts.Instance)
                ViewingComponentGUI();
        }

        MessageType ErrorLevel<T>(Module<T> ts) where T : class
        {
            if (!m_lastResult.ErrorLogs.IsNullOrEmpty() &&
                m_lastResult.ErrorLogs.FindIndex(l => ReferenceEquals(l.Context, ts.Instance)) != -1)
                return MessageType.Error;
            if (!m_lastResult.WarningLogs.IsNullOrEmpty() &&
                m_lastResult.WarningLogs.FindIndex(l => ReferenceEquals(l.Context, ts.Instance)) != -1)
                return MessageType.Warning;
            //if (!m_lastResult.InfoLogs.IsNullOrEmpty() &&
            //    m_lastResult.InfoLogs.FindIndex(l => ReferenceEquals(l.Context, ts.Instance)) != -1)
            //    return MessageType.Info;
            return MessageType.None;
        }
        #endregion

        #region Sync
        void SyncCurrentPrefab()
        {
            var prefab = m_currentStage.prefabContentsRoot;
            m_prefab = prefab;
            if (m_prefab == null)
                return;

            InitPrefabModule(ref m_archetype);

            InitPrefabModule(ref m_ctxComponent);
            InitPrefabModule(ref m_transformModule);

            InitPrefabModule(ref m_inventoryModule);
            InitPrefabModule(ref m_equipModule);

            InitPrefabModule(ref m_controlCentre);
            InitPrefabModule(ref m_energyCentre);

            InitPrefabModule(ref m_modelSpawner);
            InitPrefabModule(ref m_attachmentPoints);

            m_dirName = Path.GetDirectoryName(m_currentStage.assetPath)?.Replace('\\','/');
            m_dir = AssetDatabase.LoadAssetAtPath<DefaultAsset>(m_dirName);

            SyncAndVerify();
        }
        
        #endregion

        #region Verify
        void SyncAndVerify()
        {
            var sync = m_prefab.GetComponentsInChildren<ISyncComponents>(true);
            foreach (var s in sync)
            {
                if ((s.gameObject.hideFlags & HideFlags.DontSave) == HideFlags.DontSave)
                    continue;

                s.Editor_Sync(m_prefab);
            }            
            AssetVerificationProcessor.VerifyPrefab(m_prefab);
        }

        static void AddToDictionary<T>(IDictionary<Type, object> typeInstanceDict, Module<T> module)
        {
            if (module.Instance != null)
                typeInstanceDict.Add(module.Type, module.Instance);
        }

        void VerifyGUI()
        {
            for (var i = 0; i < m_lastResult.Errors; i++)
            {
                EditorGUILayout.HelpBox(m_lastResult.ErrorLogs[i].LogString, MessageType.Error);
                using (new EditorGUI.DisabledScope(true))
                    EditorGUILayout.ObjectField(m_lastResult.ErrorLogs[i].Context, null, true);
            }
            for (var i = 0; i < m_lastResult.Warnings; i++)
            {
                EditorGUILayout.HelpBox(m_lastResult.WarningLogs[i].LogString, MessageType.Warning);
                using (new EditorGUI.DisabledScope(true))
                    EditorGUILayout.ObjectField(m_lastResult.WarningLogs[i].Context, null, true);
            }
            for (var i = 0; i < m_lastResult.InfoLogs.Count; i++)
            {
                EditorGUILayout.HelpBox(m_lastResult.InfoLogs[i].LogString, MessageType.Info);
                using (new EditorGUI.DisabledScope(true))
                    EditorGUILayout.ObjectField(m_lastResult.InfoLogs[i].Context, null, true);
            }
        }
        public void OnEvent(PrefabVerified @event) => m_lastResult = @event.Result;
        #endregion

        #region Prefab Creation
        void NoPrefabStageGUI()
        {
            m_prefab = null;

            EditorGUILayout.HelpBox("Currently not in prefab mode", MessageType.Info);


            m_dir = (DefaultAsset) EditorGUILayout.ObjectField("Prefab-Directory ", m_dir, typeof(DefaultAsset), true);
            m_dirName = AssetDatabase.GetAssetPath(m_dir);
            m_gameObjectName = EditorGUILayout.TextField("Name: ", m_gameObjectName);

            using (new EditorGUI.DisabledScope(m_gameObjectName.IsNullOrEmpty() || m_dir == null))
            {
                if (GUILayout.Button("CreatePrefab"))
                    CreatePrefab();
            }
        }

        void CreatePrefab()
        {
            var prefabWorkingDirectory = $"{m_dirName}/{m_gameObjectName}";
            Directory.CreateDirectory($"{prefabWorkingDirectory}");
            var newGO = new GameObject(m_gameObjectName);
            var fullPath = $"{prefabWorkingDirectory}/{m_gameObjectName}.prefab";
            PrefabUtility.SaveAsPrefabAsset(newGO, fullPath, out var success);
            Debug.Log(success);
            if (success)
            {
                AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(fullPath, typeof(GameObject)));
            }
        }
        #endregion
    }

}
