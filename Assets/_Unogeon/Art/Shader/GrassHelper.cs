﻿using Core.Unity.Extensions;
using Game.Globals;
using GameUtility.Data.Visual;
using UnityEngine;

namespace Art.Shader
{
    public class GrassHelper : MonoBehaviour
    {
        public string OtherPos;
        //public string LerpVal;

        Material[] m_materialInstance;

        IMaterialInstanceProvider GetInstanceProvider()
        {
            if (!this.TryGet(out IMaterialInstanceProvider mip))
            {
                var newMip = gameObject.AddComponent<MaterialInstanceProvider>();
                newMip.SetSharedStatic(true);
                mip = newMip;
            }   
            return mip;
        }

        void Init()
        {
            if (!this.TryGet(out Renderer _))
                return;

            var provider = GetInstanceProvider();
            provider.GetInstances(out _, out m_materialInstance);
        }

        void Start() => Init();

        void Update()
        {
            if (GameGlobals.I.PlayerManager == null)
                return;
            if (GameGlobals.I.PlayerManager.PlayerCount <= 0)
                return;
            foreach (var p in GameGlobals.I.PlayerManager.Players)
                SetPosition(p.transform.position);

        }

        void SetPosition(Vector3 pos)
        {
            foreach (var mat in m_materialInstance)
            {
                //var lerpVal = mat.GetFloat(LerpVal);
                //var newLerpVal = Mathf.Clamp(Vector3.Distance(transform.position, pos), 0.01f, 1f);
                //mat.SetFloat(LerpVal, Mathf.Lerp(lerpVal, newLerpVal, 10*Time.deltaTime));
                mat.SetVector(OtherPos, pos);
            }
        }
    }
}
